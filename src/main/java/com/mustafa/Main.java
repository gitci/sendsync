package com.mustafa;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.Properties;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Log4j configuration file path: " + System.getProperty("log4j.configurationFile"));
        logger.info("Application started");

        Properties props = new Properties();
        try {
            props.load(new FileInputStream("config.properties"));
        } catch (IOException e) {
            logger.error("Could not load config.properties", e);
            return;
        }

        String directoryPath = props.getProperty("source.directory");

        logger.info("source.directory: " + directoryPath);

        SendFileService sendFileService = new SendFileService();

        WatchService watchService
                = FileSystems.getDefault().newWatchService();

        Path path = Paths.get(directoryPath);

        try {
            path.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE);
        } catch (IOException e) {
            logger.error("Could not register watch service", e);
            return;
        }

        WatchKey key;
        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                String filename = event.context().toString();
                if (filename.endsWith(".xml")) {
                    logger.info("Sending file: " + filename);
                    sendFileService.sendFile(Paths.get(directoryPath, filename).toString());
                }
            }
            key.reset();
        }

        logger.info("Application ended");
    }
}
