package com.mustafa;

import java.io.FileInputStream;
import java.util.Properties;
import okhttp3.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class SendFileService {

    private final OkHttpClient client = new OkHttpClient();
    private final Properties properties = new Properties();

    private static final Logger logger = LogManager.getLogger(SendFileService.class);

    public SendFileService() {

        try {
            String jarPath = SendFileService.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            String dirPath = new File(jarPath).getParent();
            String configPath = dirPath + File.separator + "config.properties";
            try (InputStream input = new FileInputStream(configPath)) {
                properties.load(input);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendFile(String sourceFilePath) throws IOException {
        File file = new File(sourceFilePath);

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse("application/octet-stream"), file))
                .build();

        Request request = new Request.Builder()
                .url(properties.getProperty("upload.url"))
                .post(requestBody)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            logger.info("Sent file " + sourceFilePath);
        }
    }

    public String getSourceDirectory() {
        return properties.getProperty("source.directory");
    }

}
