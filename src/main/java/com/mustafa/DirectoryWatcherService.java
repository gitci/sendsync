package com.mustafa;


import java.io.IOException;
import java.nio.file.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class DirectoryWatcherService {

    private final WatchService watcher;
    private final Path dir;

    private static final Logger logger = LogManager.getLogger(DirectoryWatcherService.class);
    public DirectoryWatcherService(Path dir) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);
        this.dir = dir;
    }

    public void processEvents() throws IOException {
        for (; ; ) {
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }

                WatchEvent<Path> ev = (WatchEvent<Path>) event;
                Path filename = ev.context();

                logger.info(filename + " has been created in the directory");
                // Call your method to send this file to another server
                SendFileService sendFileService = new SendFileService();
                sendFileService.sendFile(this.dir.resolve(filename).toString());
            }

            boolean valid = key.reset();
            if (!valid) {
                break;
            }
        }
    }


}
